package stack

import (
	"fmt"
	"sync"
)

// Safe - safe stack structure
type Safe struct {
	head *stackItem
	lock sync.Mutex
}

// Push - add item to stack
func (q *Safe) Push(obj interface{}) {
	q.lock.Lock()

	q.head = &stackItem{
		data: obj,
		next: q.head,
	}

	q.lock.Unlock()
}

// Pull - get item from stack
func (q *Safe) Pull() (interface{}, error) {
	if q.head == nil {
		return nil, fmt.Errorf("not found")
	}

	q.lock.Lock()

	elem := q.head
	q.head = q.head.next

	q.lock.Unlock()

	return elem.data, nil
}
