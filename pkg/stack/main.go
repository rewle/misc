package stack

type stackItem struct {
	data interface{}
	next *stackItem
}

// Stack - FILO structure with push and pull methods
type Stack interface {
	Push(obj interface{})
	Pull() (interface{}, error)
}

// GetStack - create Stack structure
func GetStack(safe bool) Stack {
	if safe {
		return &Safe{}
	}

	return &Unsafe{}
}
