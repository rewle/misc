package stack

import (
	"testing"
)

func TestUnsafeEmpty(t *testing.T) {
	stack := Unsafe{}
	data, err := stack.Pull()
	if err == nil {
		t.Errorf("err is nil")
	}

	if err.Error() != "not found" {
		t.Errorf("missmatch message")
	}

	if data != nil {
		t.Errorf("data not nil")
	}
}
