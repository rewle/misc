package stack

import (
	"fmt"
)

// Unsafe - unsafe stack structure
type Unsafe struct {
	head *stackItem
}

// Push - add item to stack
func (q *Unsafe) Push(obj interface{}) {
	q.head = &stackItem{
		data: obj,
		next: q.head,
	}
}

// Pull - get item from stack
func (q *Unsafe) Pull() (interface{}, error) {
	if q.head == nil {
		return nil, fmt.Errorf("not found")
	}

	elem := q.head
	q.head = q.head.next

	return elem.data, nil
}
