package main

import (
	"fmt"
	"bitbucket.org/rewle/misc/pkg/stack"
)

func main() {
	s := fifo.GetStack(false)
	s.Push(1)
	fmt.Println(s.Pull())
}